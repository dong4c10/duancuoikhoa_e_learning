import React from 'react'
import NavAdmin from '../Pages/admin/NavAdmin/NavAdmin'
import "./LayoutMain.css"
import ListCourseAdmin from '../Pages/admin/ListCourseAdmin/ListCourseAdmin'

const LayoutMain = () => {
  return (
    <div className='bg-all flex  '>
        <NavAdmin/>
        <ListCourseAdmin/>
    </div>
  )
}

export default LayoutMain