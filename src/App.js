import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import NotFound from "./Pages/NotFound/NotFound";
import LayoutMain from "./Layout/LayoutMain";
import LayoutCourse from "./Pages/admin/CourseManagement/LayoutCourse/LayoutCourse";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="admin/quanlynguoidung" element={<LayoutMain />} />
          <Route path="admin/quanlykhoahoc" element={<LayoutCourse />} />

          <Route path="/404" element={<NotFound />} />
          <Route path="*" element={<Navigate to={"/admin/quanlynguoidung"} />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
