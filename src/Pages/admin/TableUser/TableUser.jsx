import { Form, Input, Select, Space, Table, Tag } from "antd";
import { Button, Modal } from "antd";
import React, { useEffect, useState } from "react";
import Register from "./components/Register";
import {
  LockOutlined,
  MailOutlined,
  PhoneOutlined,
  SmileOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Option } from "antd/es/mentions";
import { https } from "../../../Services/config";
import Swal from "sweetalert2";
import HeaderAdmin from "../HeaderAdmin/HeaderAdmin";

export default function TableUser() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [courses, setCourses] = useState([]);
  const [list, setList] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [formEdit] = Form.useForm();

  useEffect(() => {
    https
      .get("/QuanLyNguoiDung/LayDanhSachNguoiDung?MaNhom=GP01")
      .then((res) => {
        setList(res.data);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
  }, [courses]);
  const fetchData = () => {
    https
      .get(`QuanLyNguoiDung/LayDanhSachNguoiDung`)
      .then((res) => {
        setCourses(res?.data);
        console.log("choxetduyet: ", res?.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleConfirmDelete = (taiKhoan) => {
    https
      .delete(`QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`)
      .then((res) => {
        console.log("res: ", res);
        fetchData(); // Refresh the user list
      })
      .catch((err) => {
        console.log("err: ", err);
        Swal.fire(
          err.response.data,
          "Đã xảy ra lỗi vui lòng quay lại trang chủ hoặc thử lại",
          "warning"
        );
      });
  };
  // Chức năng tìm kiếm
  const handleSearchChange = (e) => {
    setSearchValue(e.target.value);
  };
  const filteredData = list
    ? list.filter((item) => {
        const { taiKhoan, hoTen } = item;
        const searchKeyword = searchValue.toLowerCase();
        return (
          taiKhoan.toLowerCase().includes(searchKeyword) ||
          hoTen.toLowerCase().includes(searchKeyword)
        );
      })
    : [];

  // Hiện form
  const showModal = (record) => {
    setIsModalOpen(true);
    const data = { ...record, matKhau: "demo@123" };
    formEdit.setFieldsValue(data);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const onFinish = () => {
    formEdit.validateFields().then((values) => {
      const data = {
        taiKhoan: values.taiKhoan,
        hoTen: values.hoTen,
        email: values.email,
        soDt: values.soDt,
        matKhau: "demo@123",
        maNhom: "GP01",
        maLoaiNguoiDung: values.maLoaiNguoiDung,
      };
      https
        .put("QuanLyNguoiDung/CapNhatThongTinNguoiDung", data)
        .then((res) => {
          console.log("Cập nhật thành công", res.data);
          Swal.fire("Good job!", "Cập nhật thành công!", "success");
          handleOk();
          fetchData();
        })
        .catch((error) => {
          console.log(error);
        });
    });
  };
  const columns = [
    {
      title: "Tài Khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Người Dùng",
      dataIndex: "maLoaiNguoiDung",
      key: "maLoaiNguoiDung",
      render: (maLoaiNguoiDung) => {
        let color = maLoaiNguoiDung === "GV" ? "green" : "geekblue";
        if (maLoaiNguoiDung === "loser") {
          color = "volcano";
        }
        return <Tag color={color}>{maLoaiNguoiDung.toUpperCase()}</Tag>;
      },
    },

    {
      title: "Họ và Tên",
      dataIndex: "hoTen",
      key: "hoTen",
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },
    {
      title: "Số Điện Thoại",
      dataIndex: "soDt",
      key: "soDt",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            size="large"
            className="bg-green-500 text-white flex items-center"
            type="primary"
            // onClick={showModal}
          >
            <Register taiKhoan={record.taiKhoan} />
          </Button>
          <Button
            size="large"
            className="bg-yellow-400 text-white"
            type="primary"
            onClick={() => showModal(record)}
          >
            Sửa
          </Button>
          <Button
            onClick={() => handleConfirmDelete(record.taiKhoan)}
            size="large"
            className="bg-red-500 text-white"
            type="primary"
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];
  return (
    <div className="px-10 mt-10">
      <HeaderAdmin
        onSearchChange={handleSearchChange}
        searchValue={searchValue}
      />
      <Table
        className="mt-9"
        columns={columns}
        dataSource={filteredData}
        pagination={{
          pageSize: 6,
          showSizeChanger: true,
        }}
      />

      <Modal
        footer={
          <Space>
            <Button
              id="btn_close"
              className="bg-red-600 text-white hover:text-white"
              onClick={handleCancel}
            >
              Đóng
            </Button>
            <Button
              form="form_add_cource"
              type="primary"
              htmlType="submit"
              id="btn_add"
              className="bg-green-600 text-white hover:text-white"
              // onClick={() => handleEdit()}
            >
              Cập nhật
            </Button>
          </Space>
        }
        title="Cập nhật"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form
          form={formEdit}
          name="form_add_cource"
          wrapperCol={{
            span: 24,
          }}
          style={{
            maxWidth: 1000,
          }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            name="taiKhoan"
            rules={[
              {
                pattern: new RegExp(/^[a-zA-Z0-9]*$/),
                message: "Không cho phép khoảng trắng hoặc ký tự đặc biệt",
              },
              {
                required: true,
                message: "Vui lòng nhập tên người dùng của bạn!",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Tài Khoản"
            />
          </Form.Item>

          <Form.Item
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu của bạn!",
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="Mật Khẩu"
            />
          </Form.Item>
          <Form.Item
            name="email"
            rules={[
              {
                type: "email",
                required: true,
                message: "Vui lòng nhập đúng email của bạn!",
              },
            ]}
          >
            <Input
              placeholder="Email"
              prefix={<MailOutlined className="site-form-item-icon" />}
            />
          </Form.Item>
          <Form.Item
            name="hoTen"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên người Họ và Tên của bạn!",
              },
            ]}
          >
            <Input
              prefix={<SmileOutlined className="site-form-item-icon" />}
              placeholder="Họ và Tên"
            />
          </Form.Item>
          <Form.Item
            name="soDt"
            rules={[
              {
                pattern: new RegExp(/^[0-9]{9,15}$/),
                message: "Số điện thoại chưa đúng định dạng",
              },
              {
                required: true,
                message: "Vui lòng nhập số điện thoại của bạn!",
              },
            ]}
          >
            <Input
              prefix={<PhoneOutlined className="site-form-item-icon" />}
              placeholder="Số điện thoại"
            />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            rules={[{ required: true, message: "Vui lòng chọn người dùng!" }]}
          >
            <Select size="large" placeholder="Loại người dùng">
              <Option value="gv">Giáo Vụ</Option>
              <Option value="hv">Học Viên</Option>
            </Select>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
