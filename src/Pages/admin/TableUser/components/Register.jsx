import React, { useEffect, useState } from "react";
import { Button, Divider, Input, Modal, Space, Table } from "antd";
// import RegisterUser from "./RegisterUser";
import { https } from "../../../../Services/config";
import Swal from "sweetalert2";

export default function Register({ taiKhoan }) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [courses, setCourses] = useState([]);
  const [courses1, setCourses1] = useState([]);

  const taiKhoan1 = { taiKhoan };

  const fetchData = () => {
    https
      .post(`QuanLyNguoiDung/LayDanhSachKhoaHocChoXetDuyet`, taiKhoan1)
      .then((res) => {
        setCourses(res?.data);
        console.log("choxetduyet: ", res?.data);
      })
      .catch((error) => {
        console.log(error);
      });

    https
      .post(`QuanLyNguoiDung/LayDanhSachKhoaHocDaXetDuyet`, taiKhoan1)
      .then((res) => {
        setCourses1(res?.data);
        console.log("daxetduyet: ", res?.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleDelete = (record) => {
    https
      .post(`QuanLyKhoaHoc/HuyGhiDanh`, {
        maKhoaHoc: record.maKhoaHoc,
        taiKhoan,
      })
      .then((res) => {
        setCourses((prevCourses) =>
          prevCourses.filter((course) => course.maKhoaHoc !== record.maKhoaHoc)
        );
        Swal.fire("Good job!", "Xóa thành công!", "success");
      })
      .catch((error) => {
        console.log(error);
      });
      fetchData();
  };

  const handleRegister = (record) => {
    https
      .post(`QuanLyKhoaHoc/GhiDanhKhoaHoc`, {
        maKhoaHoc: record.maKhoaHoc,
        taiKhoan,
      })
      .then((res) => {
        Swal.fire("Good job!", "Ghi danh thành công!", "success");
        fetchData();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const showModal = () => {
    fetchData();
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // const taiKhoan = "vin999";
  const columns = [
    {
      title: "STT",
      dataIndex: "key",
      key: "key",
      // render: (text) => <a>{text}</a>,
    },
    {
      title: "Tên khóa học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Chờ xác nhận",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            size="large"
            onClick={() => handleDelete(record)}
            className="bg-red-500 text-white"
            type="primary"
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];
  const columns1 = [
    {
      title: "STT",
      dataIndex: "key",
      key: "key",
      // render: (text) => <a>{text}</a>,
    },
    {
      title: "Tên khóa học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Chờ xác nhận",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            size="large"
            className="bg-green-500 text-white"
            type="primary"
            onClick={() => handleRegister(record)}
          >
            Xác thực
          </Button>
          <Button
            onClick={() => handleDelete()}
            size="large"
            className="bg-red-500 text-white"
            type="primary"
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Ghi Danh
      </Button>
      <Modal
        footer={null}
        // title="Basic Modal"
        open={isModalOpen}
        onCancel={handleCancel}
        width={1000}
      >
        <div className="flex items-center gap-14">
          <p className="text-xl font-semibold w-full">Chọn khóa học</p>
          <Input
            className="w-full ml-auto rounded"
            size="large"
            placeholder="Chọn khóa học"
          />

          <Button
            size="large"
            className="bg-green-500 text-white mr-7"
            type="primary"
          >
            Ghi Danh
          </Button>
        </div>
        <Divider></Divider>
        <div>
          <div>
            <p className="text-xl font-semibold w-full">
              Khóa học chờ xác thực
            </p>
            <Table
              taiKhoan={taiKhoan}
              className="pt-4"
              columns={columns1}
              dataSource={courses.map((course, index) => ({
                ...course,
                key: index + 1,
              }))}
              pagination={{ pageSize: 2 }}
            />
          </div>
        </div>
        <Divider></Divider>
        <div>
          <p className="text-xl font-semibold w-full">Khóa học đã ghi danh</p>
          <Table
            taiKhoan={taiKhoan}
            className="pt-4 "
            columns={columns}
            dataSource={courses1.map((course, index) => ({
              ...course,
              key: index + 1,
            }))}
            pagination={{ pageSize: 2 }}
          />
        </div>
      </Modal>
    </>
  );
}
