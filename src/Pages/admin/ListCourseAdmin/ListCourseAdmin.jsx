import React from "react";
// import HeaderAdmin from "../HeaderAdmin/HeaderAdmin";
import TableUser from "../TableUser/TableUser";

export default function ListCourseAdmin() {
  return (
    <div className="rounded-md w-full ml-2 mr-6 my-4 bg-white pt-2">
      <div >
        {/* <HeaderAdmin/> */}
        <TableUser/>
      </div>
    </div>
  );
}
