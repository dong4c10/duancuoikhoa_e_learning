import React, { useState } from "react";
import { Button, Dropdown, Form, Modal, Space, Input, Select } from "antd";
import {
  DownOutlined,
  LockOutlined,
  MailOutlined,
  PhoneOutlined,
  SmileOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { https } from "../../../Services/config";
import Swal from "sweetalert2";
import { useDispatch } from "react-redux";
import { update } from "../../../redux/userSlice";

const { Option } = Select;

export default function HeaderAdmin({ onSearchChange, searchValue }) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalOpen2, setIsModalOpen2] = useState(false);
  const [formAdd] = Form.useForm();
  const dispatch = useDispatch();
  const [selectedUser, setSelectedUser] = useState(null);
  const [formEdit] = Form.useForm();
  const [user, setUser] = useState({
    taiKhoan: "",
    matKhau: "",
    hoTen: "",
    soDT: "",
    maLoaiNguoiDung: "",
    maNhom: "GP01",
    email: "",
  });

  const handleChange = (value) => {
    setUser((prevProduct) => ({
      ...prevProduct,
      ...value,
    }));
  };
  console.log("Updated user:", user);
  const onFinish = (e) => {
    // console.log("Success:", values);
    e.preventDefault();
    console.log("Form values:", formAdd.getFieldsValue());
    https
      .post("QuanLyNguoiDung/ThemNguoiDung", user)
      .then((res) => {
        console.log("res: ", res.data);
        // formAdd.resetFields();
        // setList(res.data);
        setUser({
          taiKhoan: "",
          matKhau: "",
          hoTen: "",
          soDT: "",
          maLoaiNguoiDung: "",
          maNhom: "",
          email: "",
        });
        console.log("setUser: ", setUser);
        Swal.fire("Good job!", "Thêm thành công!", "success");
        setIsModalOpen(false);
      })
      .catch((err) => {
        console.log("err: ", err);
        Swal.fire("Oops...", "Có lỗi xảy ra khi thêm người dùng!", "error");
      });
  };
  const showModal = () => {
    setIsModalOpen(true);
  };
  // const handleOk = () => {
  //   setIsModalOpen(false);
  // };
  const showModal2 = (record) => {
    console.log('record: ', record);
    setIsModalOpen2(true);
    const data = { ...record, matKhau: "demo@123" };
    console.log("data: ", data);
    formEdit.setFieldsValue(data);
  };

  const handleCancel2 = () => {
    setIsModalOpen2(false);
  };

  const items = [
    {
      key: "1",
      label: (
        <button
          target="_blank"
          rel="noopener noreferrer"
          onClick={() => showModal2()}
        >
          Cập nhật thông tin
        </button>
      ),
    },
    {
      key: "2",
      danger: true,
      label: "Đăng xuất",
    },
  ];

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const updateInfo = () => {
    formEdit.validateFields().then((value) => {
      const data = {
        taiKhoan: value.taiKhoan,
        hoTen: value.hoTen,
        email: value.email,
        soDt: value.soDt,
        matKhau: "demo@123",
        maNhom: "GP01",
        maLoaiNguoiDung: value.maLoaiNguoiDung,
      };
      https
        .put("api/QuanLyNguoiDung/CapNhatThongTinNguoiDung", data)
        .then((res) => {
          // dispatch(update(data));
          Swal.fire("Cập Nhật Thành Công", " Thành công!", "success");
          setTimeout(() => {
            handleCancel();
          }, 1000);
        })
        .catch((err) => {});
    });
  };
  

  return (
    <div className="flex w-full items-center mt-6">
      <div className="ml-12">
        <Button
          size="large"
          className="bg-green-500"
          type="primary"
          onClick={() =>showModal}
        >
          Thêm người dùng
        </Button>
      </div>

      <div className="flex items-center w-full gap-24">
        <Input
          onChange={onSearchChange}
          value={searchValue}
          className="w-1/2 ml-auto rounded"
          size="large"
          placeholder="Nhập vào tài khoản hoặc họ tên người dùng"
        />
        <div className="flex items-center gap-5 loginInfo mr-14">
          <span className="text-lg font-bold">Xuân Đông</span>
          <Dropdown menu={{ items }}>
            <a href="/" onClick={(e) => e.preventDefault()}>
              <Space>
                <img
                  className="rounded-full "
                  src="https://cdn.dribbble.com/users/2364329/screenshots/6676961/02.jpg?compress=1&resize=800x600"
                  width={70}
                  height={90}
                  alt=""
                />
                <DownOutlined />
              </Space>
            </a>
          </Dropdown>
        </div>
      </div>
      <div></div>
      <Modal
        title="Thông tin người dùng"
        open={isModalOpen}
        // onOk={handleOk}
        onCancel={handleCancel}
        footer={
          <Space>
            <Button
              id="btn_close"
              className="bg-red-600 text-white hover:text-white"
              onClick={handleCancel}
            >
              Đóng
            </Button>
            <Button
              form="form_add_cource"
              type="primary"
              htmlType="submit"
              id="btn_add"
              className="bg-green-600 text-white hover:text-white"
              onClick={onFinish}
            >
              Thêm người dùng
            </Button>
          </Space>
        }
      >
        <Form
          form={formAdd}
          name="form_add_cource"
          wrapperCol={{
            span: 24,
          }}
          style={{
            maxWidth: 1000,
          }}
          autoComplete="off"
          onValuesChange={handleChange}
        >
          <Form.Item
            name="taiKhoan"
            value={user.taiKhoan}
            rules={[
              {
                pattern: new RegExp(/^[a-zA-Z0-9]*$/),
                message: "Không cho phép khoảng trắng hoặc ký tự đặc biệt",
              },
              {
                required: true,
                message: "Vui lòng nhập tên người dùng của bạn!",
              },
            ]}
          >
            <Input
              name="taiKhoan"
              prefix={<UserOutlined className="site-form-item-icon" />}
              placeholder="Tài khoản"
            />
          </Form.Item>

          <Form.Item
            name="matKhau"
            value={user.matKhau}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập mật khẩu của bạn!",
              },
            ]}
          >
            <Input.Password
              name="matKhau"
              prefix={<LockOutlined className="site-form-item-icon" />}
              placeholder="Mật Khẩu"
            />
          </Form.Item>
          <Form.Item
            name="email"
            value={user.email}
            rules={[
              {
                type: "email",
                required: true,
                message: "Vui lòng nhập đúng email của bạn!",
              },
            ]}
          >
            <Input
              name="email"
              placeholder="Email"
              prefix={<MailOutlined className="site-form-item-icon" />}
            />
          </Form.Item>
          <Form.Item
            name="hoTen"
            value={user.hoTen}
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên người Họ và Tên của bạn!",
              },
            ]}
          >
            <Input
              name="hoTen"
              prefix={<SmileOutlined className="site-form-item-icon" />}
              placeholder="Họ và Tên"
            />
          </Form.Item>
          <Form.Item
            name="soDT"
            value={user.soDT}
            rules={[
              {
                pattern: new RegExp(/^[0-9]{9,15}$/),
                message: "Số điện thoại chưa đúng định dạng",
              },
              {
                required: true,
                message: "Vui lòng nhập số điện thoại của bạn!",
              },
            ]}
          >
            <Input
              name="soDT"
              prefix={<PhoneOutlined className="site-form-item-icon" />}
              placeholder="Số điện thoại"
            />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            value={user.maLoaiNguoiDung}
            rules={[{ required: true, message: "Vui lòng chọn người dùng!" }]}
          >
            <Select
              name="maLoaiNguoiDung"
              size="large"
              placeholder="Loại người dùng"
            >
              <Option value="GV">Giáo Vụ</Option>
              <Option value="HV">Học Viên</Option>
            </Select>
          </Form.Item>
        </Form>
      </Modal>
      {/* Form sửa thông tin người dùng  */}
      <Modal
        className="modal"
        open={isModalOpen2}
        onCancel={handleCancel2}
        footer={null}
      >
        <p className="text-2xl text-white text-center py-2 uppercase">
          thông tin cá nhân
        </p>
        <Form
          form={formEdit}
          autoComplete="off"
          name="form_add_cource"
          onFinish={updateInfo}
        >
          <Form.Item initialValue={user.taiKhoan} name="taiKhoan">
            <Input
              disabled
              prefix={<SmileOutlined className="mr-2" />}
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="matKhau"
            rules={[
              {
                required: "true",
                message: "Mật khẩu không được bỏ trống",
              },
              {
                // pattern: regex.password,
                message:
                  "Mật khẩu gồm 8-15 ký tự, 1 ký tự viết hoa, 1 số, 1 ký tự đặc biệt",
              },
            ]}
          >
            <Input.Password
              prefix={<LockOutlined className="mr-2" />}
              placeholder="Mật Khẩu"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.hoTen}
            name="hoTen"
            rules={[
              {
                required: "true",
                message: "Họ tên không được bỏ trống",
              },
            ]}
          >
            <Input
              prefix={<UserOutlined className="mr-2" />}
              placeholder="Họ và tên"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.email}
            name="email"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                type: "email",
                message: "Nhập đúng định dạng email",
              },
            ]}
          >
            <Input
              prefix={<MailOutlined className="mr-2" />}
              placeholder="Email"
              size="large"
            />
          </Form.Item>
          <Form.Item
            initialValue={user.soDT}
            name="soDt"
            rules={[
              {
                required: "true",
                message: "Email không được bỏ trống",
              },
              {
                // pattern: regex.phone,
                message: "Nhập đúng định dạng số điện thoại",
              },
            ]}
          >
            <Input
              prefix={<PhoneOutlined className="mr-2" />}
              placeholder="Số điện thoại"
              size="large"
            />
          </Form.Item>
          <Form.Item
            name="maLoaiNguoiDung"
            rules={[
              {
                required: "true",
                message: "Vui lòng chọn loại người dùng",
              },
            ]}
          >
            <Select placeholder="Loại người dùng" size="large">
              <Select.Option value="GV">Giáo Vụ</Select.Option>
              <Select.Option value="HV">Học Viên</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item
            wrapperCol={{
              offset: 10,
              span: 5,
            }}
          >
            <Button
              form="form_add_cource"
              htmlType="submit"
              size="large"
              className="text-white bg-green-400"
            >
              {" "}
              Cập nhật
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
}
