import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import {
  faBriefcase,
  faHouse,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import "./Nav.css";
import { Link } from "react-router-dom";
export default function NavAdmin() {
  return (
    <div className="pt-4 pl-6 bg-all bg-slate-500 text-center wrapper align-middle">
      <nav id="sidebar" className="align-middle">
        <Link className="btn btn-light my-6" href="" type="button">
          <span className="py-6">
            <FontAwesomeIcon className="icon_user text-2xl" icon={faHouse} />
          </span>
        </Link>
        <ul className="py-9">
          <li className="py-6  text-xl font-semibold text-black">
            <Link
              className="btn_icon flex flex-wrap justify-center"
              to={"/admin/quanlynguoidung"}
            >
              <FontAwesomeIcon className="text-2xl" icon={faUser} />
              <span className="icon-text mt-3">Quản lý người dùng</span>
            </Link>
          </li>
          <li className="mt-16 text-xl font-semibold text-black">
            <Link
              to={"/admin/quanlykhoahoc"}
              className="btn_icon flex flex-wrap justify-center"
            >
              <FontAwesomeIcon className="text-2xl" icon={faBriefcase} />
              <span className="icon-text mt-3">Quản lý khoá học</span>
            </Link>
          </li>
        </ul>
      </nav>
    </div>
  );
}
