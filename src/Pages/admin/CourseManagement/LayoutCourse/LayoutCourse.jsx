import React from "react";
import NavAdmin from "../../NavAdmin/NavAdmin";
import ListCourse from "../ListCourse/ListCourse";

export default function LayoutCourse() {
  return (
    <div className="bg-all flex ">
      <NavAdmin />
      <ListCourse />
    </div>
  );
}
