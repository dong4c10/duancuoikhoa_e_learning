import React, { useEffect, useState } from "react";
import { Button, Divider, Input, Modal, Space, Table } from "antd";
import { https } from "../../../../../Services/config";
import Swal from "sweetalert2";

export default function RegisterCourse({ maKhoaHoc, sendDataToParent }) {
  // console.log("maKhoaHoc: ", maKhoaHoc);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [courses, setCourses] = useState([]);
  const [courses1, setCourses1] = useState([]);

  const maKhoaHoc1 = { maKhoaHoc };


  const data = courses1;

  useEffect(() => {
    // Gọi hàm callback từ component cha và truyền dữ liệu
    sendDataToParent(data);
  }, [data, sendDataToParent]);

  const fetchData = () => {
    https
      .post(`QuanLyNguoiDung/LayDanhSachHocVienChoXetDuyet`, maKhoaHoc1)
      .then((res) => {
        setCourses(res?.data);
        // console.log("hocvienchoxetduyet: ", res?.data);
      })
      .catch((error) => {
        console.log(error);
      });

    https
      .post(`QuanLyNguoiDung/LayDanhSachHocVienKhoaHoc`, maKhoaHoc1)
      .then((res) => {
        setCourses1(res?.data);
        // console.log("hocviendaxetduyet: ", res?.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleDelete = (record) => {
    https
      .post(`QuanLyKhoaHoc/HuyGhiDanh`, {
        taiKhoan: record.taiKhoan,
        maKhoaHoc,
      })
      .then((res) => {
        setCourses((prevCourses) =>
          prevCourses.filter((course) => course.taiKhoan !== record.taiKhoan)
        );
        fetchData();
        Swal.fire("Good job!", "Xóa thành công!", "success");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const handleRegister = (record) => {
    https
      .post(`QuanLyKhoaHoc/GhiDanhKhoaHoc`, {
        taiKhoan: record.taiKhoan,
        maKhoaHoc,
      })
      .then((res) => {
        fetchData();
        Swal.fire("Good job!", "Ghi danh thành công!", "success");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const columns = [
    {
      title: "STT",
      dataIndex: "key",
      key: "key",
      // render: (text) => <a>{text}</a>,
    },
    {
      title: "Tài khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Học viên",
      dataIndex: "hocVien",
      key: "hocVien",
    },
    {
      title: "Chờ xác nhận",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            size="large"
            className="bg-green-500 text-white"
            type="primary"
            onClick={() => handleRegister(record)}
          >
            Xác thực
          </Button>

          <Button
            onClick={() => handleDelete(record)}
            size="large"
            className="bg-red-500 text-white"
            type="primary"
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];
  const columns1 = [
    {
      title: "STT",
      dataIndex: "key",
      key: "key",
      // render: (text) => <a>{text}</a>,
    },
    {
      title: "Tài khoản",
      dataIndex: "taiKhoan",
      key: "taiKhoan",
    },
    {
      title: "Học viên",
      dataIndex: "hocVien",
      key: "hocVien",
    },
    {
      title: "Chờ xác nhận",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button
            onClick={() => handleDelete(record)}
            size="large"
            className="bg-red-500 text-white"
            type="primary"
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];

  return (
    <>
      <Button type="primary" onClick={showModal}>
        Ghi Danh
      </Button>
      <Modal
        footer={null}
        // title="Basic Modal"
        open={isModalOpen}
        onCancel={handleCancel}
        width={1000}
      >
        <div className="flex items-center gap-14">
          <p className="text-xl font-semibold w-full">Chọn người dùng</p>
          <Input
            className="w-full ml-auto rounded"
            size="large"
            placeholder="Tên người dùng"
          />

          <Button
            size="large"
            className="bg-green-500 text-white mr-7"
            type="primary"
          >
            Ghi Danh
          </Button>
        </div>
        <Divider></Divider>
        <div>
          <div className="flex items-center gap-14">
            <p className="text-xl font-semibold w-full">
              Học viên chờ xác thực
            </p>
            <Input
              className="w-full ml-auto rounded"
              size="large"
              placeholder="Nhập tên học viên hoặc số điện thoại"
            />
          </div>

          <Table
            maKhoaHoc={maKhoaHoc}
            className="pt-4"
            columns={columns}
            dataSource={courses.map((course, index) => ({
              ...course,
              key: index + 1,
            }))}
            pagination={{ pageSize: 2 }}
          />
        </div>
        <Divider></Divider>
        <div>
          <div className="flex items-center gap-14">
            <p className="text-xl font-semibold w-full">
              Học viên đã tham gia học
            </p>
            <Input
              className="w-full ml-auto rounded"
              size="large"
              placeholder="Nhập tên học viên hoặc số điện thoại"
            />
          </div>
          <Table
            maKhoaHoc={maKhoaHoc}
            className="pt-4 "
            columns={columns1}
            dataSource={courses1.map((course, index) => ({
              ...course,
              key: index + 1,
            }))}
            pagination={{ pageSize: 2 }}
          />
        </div>
      </Modal>
    </>
  );
}
