import { Divider, Form, Input, Select, Space, Table, Upload } from "antd";
import { Button, Modal } from "antd";
import React, { useEffect, useState } from "react";
import RegisterCourse from "./components/RegisterCourse";
import TextArea from "antd/es/input/TextArea";
import { Option } from "antd/es/mentions";
import {
  AntDesignOutlined,
  CalendarOutlined,
  EyeOutlined,
  SmileOutlined,
  StarOutlined,
  TeamOutlined,
  UploadOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { https } from "../../../../Services/config";
import Swal from "sweetalert2";
import HeaderCourse from "../HeaderCourse/HeaderCourse";

export default function TableCourse() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [courses, setCourses] = useState([]);
  const [muckhoahoc, setMucKhoaHoc] = useState([]);
  const [tenDanhMucKhoaHoc, setTenDanhMucKhoaHoc] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [list, setList] = useState([]);
  const [formEdit] = Form.useForm();

  useEffect(() => {
    https
      .get("QuanLyKhoaHoc/LayDanhSachKhoaHoc")
      .then((res) => {
        // console.log("res: ", res.data);
        setList(res.data);
      })
      .catch((err) => {
        console.log("err: ", err);
      });
    https
      .get("QuanLyKhoaHoc/LayDanhMucKhoaHoc")
      .then((res) => setMucKhoaHoc(res.data))
      .catch((erorr) => {
        console.log("erorr", erorr);
      });
  }, [courses]);
  const fetchData = () => {
    https
      .get(`QuanLyKhoaHoc/LayDanhSachKhoaHoc`)
      .then((res) => {
        setList(res.data);
        // console.log("khoxetduyet: ", res?.data);
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const [receivedData, setReceivedData] = useState(null);

  // Hàm xử lý dữ liệu từ component con
  const handleDataFromChild = (data) => {
    setReceivedData(data);
  };
  const handleDelete = (record) => {
    console.log("receivedData: ", receivedData.length);

    console.log("record.maKhoaHoc: ", record.maKhoaHoc);
    if (receivedData.length === 0) {
      https
        .delete(`QuanLyKhoaHoc/XoaKhoaHoc?maKhoaHoc=${record.maKhoaHoc}`)
        .then((res) => {
          fetchData();
          console.log("khoxetduyet: ", res?.data);
          Swal.fire(
            "Xóa khóa học thành công",
            "Chúc mừng bạn đã xóa khóa học thành công.",
            "success"
          );
        })
        .catch((error) => {
          console.log(error);
        });
      // }
    } else {
      Swal.fire(
        "Không thể xóa khóa học",
        "Bảng khóa học không rỗng. Vui lòng xóa hết khóa học trước khi xóa.",
        "warning"
      );
    }
  };
  const handleSearchChange = (e) => {
    setSearchValue(e.target.value);
  };
  const filteredData = list.filter((item) => {
    const { tenKhoaHoc } = item;
    const searchKeyword = searchValue.toLowerCase();
    return tenKhoaHoc.toLowerCase().includes(searchKeyword);
  });
  const showModal = (record) => {
    setIsModalOpen(true);
    const data = { ...record };
    setTenDanhMucKhoaHoc(data.danhMucKhoaHoc.tenDanhMucKhoaHoc);
    formEdit.resetFields();
    formEdit.setFieldsValue(data);
  };
  const handleChange = (value) => {
    if (value.hinhAnh) {
      value.hinhAnh = value.hinhAnh.file;
    }

    setCourses((prevProduct) => ({
      ...prevProduct,
      ...value,
    }));
  };
  const uploadProps = {
    multiple: false,
    action: "https://localhost:3000/",
    accept: ".png,.jpg,.doc",
    listType: "picture",
    beforeUpload: (file) => {
      return false;
    },
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleEditImg = (value) => {
    console.log("giá trị: ", value);
    formEdit.validateFields().then(() => {
      const formData = new FormData();
      formData.append("maKhoaHoc", value.maKhoaHoc);
      formData.append("biDanh", value.biDanh);
      formData.append("tenKhoaHoc", value.tenKhoaHoc);
      formData.append("moTa", value.moTa);
      formData.append("luotXem", value.luotXem);
      formData.append("danhGia", value.danhGia);
      formData.append("maNhom", value.maNhom);
      formData.append("ngayTao", value.ngayTao);
      formData.append("maDanhMucKhoaHoc", value.maDanhMucKhoaHoc);
      formData.append("taiKhoanNguoiTao", value.taiKhoanNguoiTao);
      // Append additional form fields if needed

      // Check if the file input has a value
      if (value.hinhAnh && value.hinhAnh.file) {
        formData.append("hinhAnh", value.hinhAnh.file);
      }

      https
        .post("QuanLyKhoaHoc/CapNhatKhoaHocUpload", formData)
        .then((data) => {
          setCourses(data);
          console.log("Cập nhật thành công", data);
          Swal.fire("Làm tốt lắm!", "Cập nhật thành công!", "success");
          handleOk();
          fetchData();
        })
        .catch((error) => {
          console.log(error);
          Swal.fire(
            "Không tìm thấy khóa học!",
            "Đã xảy ra lỗi vui lòng quay lại trang chủ hoặc thử lại!",
            "warning"
          );
        });
    });
  };

  const onFinish = () => {
    formEdit.validateFields().then((values) => {
      console.log("values: ", values);
      const data = {
        maKhoaHoc: values.maKhoaHoc,
        biDanh: values.biDanh,
        tenKhoaHoc: values.tenKhoaHoc,
        moTa: values.moTa,
        luotXem: values.luotXem,
        danhGia: values.danhGia,
        hinhAnh: values.hinhAnh.file,
        maNhom: "GP01",
        ngayTao: values.ngayTao,
        maDanhMucKhoaHoc: values.maDanhMucKhoaHoc,
        taiKhoanNguoiTao: values.taiKhoanNguoiTao,
      };
      https
        .put("QuanLyKhoaHoc/CapNhatKhoaHoc", data)
        .then((res) => {
          // setCourses(res?.data);
          console.log("Cập nhật thành công", res?.data);
          Swal.fire("Good job!", "Cập nhật thành công!", "success");
          handleOk();
          fetchData();
        })
        .catch((error) => {
          console.log(error.response);
        });
    });
  };

  const columns = [
    {
      title: "Mã khóa học",
      dataIndex: "maKhoaHoc",
      key: "maKhoaHoc",
    },
    {
      title: "Tên khóa học",
      dataIndex: "tenKhoaHoc",
      key: "tenKhoaHoc",
    },
    {
      title: "Hình ảnh",
      dataIndex: "hinhAnh",
      key: "hinhAnh",
      render: (hinhAnh) => {
        //console.log(hinhAnh); // Thêm dòng này để log hinhAnh
        return <img width={80} src={hinhAnh} alt="" />;
      },
    },
    {
      title: "Lượt xem",
      dataIndex: "luotXem",
      key: "luotXem",
    },
    {
      title: "Người tạo",
      dataIndex: "nguoiTao",
      key: "nguoiTao.hoTen",
      render: (nguoiTao) => {
        return <span>{nguoiTao.hoTen}</span>;
      },
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle" className="m-auto">
          <Button
            size="large"
            className="bg-green-500 text-white flex items-center"
            type="primary"
            // onClick={showModal}
          >
            <RegisterCourse
              maKhoaHoc={record.maKhoaHoc}
              sendDataToParent={handleDataFromChild}
            />
          </Button>
          <Button
            size="large"
            className="bg-yellow-400 text-white"
            type="primary"
            onClick={() => showModal(record)}
          >
            Sửa
          </Button>
          <Button
            onClick={() => handleDelete(record)}
            size="large"
            className="bg-red-500 text-white"
            type="primary"
          >
            Xóa
          </Button>
        </Space>
      ),
    },
  ];
  return (
    <div className="px-10 mt-10">
      <HeaderCourse
        onSearchChange={handleSearchChange}
        searchValue={searchValue}
      />
      <Table
        className="mt-9"
        columns={columns}
        dataSource={filteredData}
        pagination={{ pageSize: 4 }}
      />
      <Modal
        width={1000}
        title="Sửa khóa học"
        open={isModalOpen}
        // onOk={handleOk}
        onCancel={handleCancel}
        footer={
          <Space>
            <Button
              id="btn_close"
              className="bg-red-600 text-white hover:text-white"
              onClick={handleCancel}
            >
              Đóng
            </Button>
            <Button
              form="form_add_cource"
              type="primary"
              htmlType="submit"
              id="btn_add"
              className="bg-green-600 text-white hover:text-white"
              onClick={() => onFinish()}
            >
              Cập nhật khóa học
            </Button>
          </Space>
        }
      >
        <div>
          <Form
            onValuesChange={handleChange}
            form={formEdit}
            className=" grid grid-cols-2 gap-7"
            name="form_add_cource"
            wrapperCol={{
              span: 24,
            }}
            style={{
              maxWidth: 955,
            }}
            onFinish={handleEditImg}
            autoComplete="off"
          >
            <div>
              <Form.Item
                name="maKhoaHoc"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mã khóa học!",
                  },
                ]}
              >
                <Input
                  // name="maKhoaHoc"
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Mã khóa học"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="maDanhMucKhoaHoc"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn danh mục khóa học!",
                  },
                ]}
              >
                <Select
                  // name="maDanhMucKhoaHoc"
                  size="large"
                  prefix={<TeamOutlined className="site-form-item-icon" />}
                  placeholder="Danh mục khóa học"
                  defaultValue={tenDanhMucKhoaHoc}
                >
                  {muckhoahoc.map((item) => (
                    <Select.Option key={item.id} value={item.maDanhMuc}>
                      {item.tenDanhMuc} - {item.maDanhMuc}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="danhGia"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập đánh giá!",
                  },
                ]}
              >
                <Input
                  // name="danhGia"
                  placeholder="Đánh giá"
                  prefix={<StarOutlined className="site-form-item-icon" />}
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="taiKhoanNguoiTao"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập người tạo!",
                  },
                ]}
              >
                <Input
                  // name="taiKhoanNguoiTao"
                  prefix={<SmileOutlined className="site-form-item-icon" />}
                  placeholder="Người tạo"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="tenKhoaHoc"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tên khóa học!",
                  },
                ]}
              >
                <Input
                  // name="tenKhoaHoc"
                  prefix={<AntDesignOutlined className="site-form-item-icon" />}
                  placeholder="Tên khóa học"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="ngayTao"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập ngày tạo!",
                  },
                ]}
              >
                <Input
                  // name="ngayTao"
                  prefix={<CalendarOutlined className="site-form-item-icon" />}
                  placeholder="Ngay tao"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="luotXem"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập lượt xem!",
                  },
                ]}
              >
                <Input
                  // name="luotXem"
                  prefix={<EyeOutlined className="site-form-item-icon" />}
                  placeholder="Lượt xem"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item name="hinhAnh" label="Hình Ảnh">
                <Upload {...uploadProps}>
                  <Button icon={<UploadOutlined />}>Click to upload</Button>
                </Upload>
              </Form.Item>
            </div>
            <div>
              <Form.Item
                name="maNhom"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn mã nhóm!",
                  },
                ]}
              >
                <Select
                  // name="maNhom"
                  size="large"
                  prefix={<TeamOutlined className="site-form-item-icon" />}
                  placeholder="Mã nhóm "
                >
                  <Option value="GP01">GP01</Option>
                  <Option value="GP02">GP02</Option>
                  <Option value="GP03">GP03</Option>
                  <Option value="GP04">GP04</Option>
                  <Option value="GP05">GP05</Option>
                  <Option value="GP06">GP06</Option>
                  <Option value="GP07">GP07</Option>
                  <Option value="GP08">GP08</Option>
                  <Option value="GP09">GP09</Option>
                  <Option value="GP10">GP10</Option>
                  <Option value="GP11">GP11</Option>
                  <Option value="GP12">GP12</Option>
                  <Option value="GP13">GP13</Option>
                  <Option value="GP14">GP14</Option>
                  <Option value="GP15">GP15</Option>
                </Select>
              </Form.Item>
            </div>
            <div>
              <Form.Item
                name="biDanh"
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập bí danh!",
                  },
                ]}
              >
                <Input
                  // name="biDanh"
                  prefix={<SmileOutlined className="site-form-item-icon" />}
                  placeholder="Bí danh"
                />
              </Form.Item>
            </div>

            <div className="w-full block">
              <div className="bg-slate-100 h-14 flex items-center">
                <p className="ml-7 font-semibold text-xl">Mô tả khóa học</p>
              </div>
              <Divider></Divider>
              <div className="flex gap-6 items-center">
                <img
                  src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png"
                  width={220}
                  alt="aa"
                />

                <Form.Item name="moTa" className="w-full ">
                  <TextArea rows={9} />
                </Form.Item>
              </div>
            </div>
          </Form>
        </div>
      </Modal>
    </div>
  );
}
