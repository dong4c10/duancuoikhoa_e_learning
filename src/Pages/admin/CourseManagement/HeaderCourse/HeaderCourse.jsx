import React, { useEffect, useState } from "react";
import {
  Button,
  Dropdown,
  Form,
  Modal,
  Space,
  Input,
  Select,
  Upload,
  Divider,
} from "antd";
import {
  AntDesignOutlined,
  CalendarOutlined,
  DownOutlined,
  EyeOutlined,
  SmileOutlined,
  StarOutlined,
  TeamOutlined,
  UploadOutlined,
  UserOutlined,
} from "@ant-design/icons";
import TextArea from "antd/es/input/TextArea";
import { https } from "../../../../Services/config";
import Swal from "sweetalert2";
const { Option } = Select;

const items = [
  {
    key: "1",
    label: (
      <a
        target="_blank"
        rel="noopener noreferrer"
        href="https://www.antgroup.com"
      >
        Cập nhật thông tin
      </a>
    ),
  },
  {
    key: "2",
    danger: true,
    label: "Đăng xuất",
  },
];
export default function HeaderCourse({ onSearchChange, searchValue }) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [formAdd] = Form.useForm();
  const [muckhoahoc, setMucKhoaHoc] = useState([]);
  const [course, setCourse] = useState({
    maKhoaHoc: "",
    biDanh: "",
    tenKhoaHoc: "",
    moTa: "",
    luotXem: 0,
    danhGia: 0,
    hinhAnh: "",
    maNhom: "GP01",
    ngayTao: "",
    maDanhMucKhoaHoc: "",
    taiKhoanNguoiTao: "",
  });
  const handleChange = (value) => {
    if (value.hinhAnh) {
      value.hinhAnh = value.hinhAnh.file;
    }

    setCourse((prevProduct) => ({
      ...prevProduct,
      ...value,
    }));
  };

  //  const props = {
  //     listType: "picture",
  //     beforeUpload(file) {
  //        return new Promise((resolve) => {
  //           const reader = new FileReader();
  //           reader.readAsDataURL(file);
  //           reader.onload = () => {
  //              const img = document.createElement("img");
  //              img.src = reader.result;
  //              img.onload = () => {
  //                 const canvas = document.createElement("canvas");
  //                 canvas.width = img.naturalWidth;
  //                 canvas.height = img.naturalHeight;
  //                 const ctx = canvas.getContext("2d");
  //                 ctx.drawImage(img, 0, 0);
  //                 ctx.fillStyle = "red";
  //                 ctx.textBaseline = "middle";
  //                 ctx.font = "33px Arial";
  //                 ctx.fillText("Ant Design", 20, 20);
  //                 canvas.toBlob((result) => resolve(result));
  //              };
  //           };
  //        });
  //     },
  //  };

  const onFinish = (e) => {
    e.preventDefault();

    const formData = new FormData();
    for (const [key, value] of Object.entries(course)) {
      formData.append(key, value);
    }

    https
      .post("QuanLyKhoaHoc/ThemKhoaHocUploadHinh", formData)
      .then((res) => {
        setCourse({
          maKhoaHoc: "",
          biDanh: "",
          tenKhoaHoc: "",
          moTa: "",
          luotXem: 0,
          danhGia: 0,
          hinhAnh: "",
          maNhom: "",
          ngayTao: "",
          maDanhMucKhoaHoc: "",
          taiKhoanNguoiTao: "",
        });

        Swal.fire("Good job!", "Thêm thành công!", "success");
        setIsModalOpen(false);
      })
      .catch((err) => {
        Swal.fire("Oops...", "Có lỗi xảy ra khi thêm khóa học!", "error");
      });
    // formAdd.resetFields();
  };
  useEffect(() => {
    https
      .get("QuanLyKhoaHoc/LayDanhMucKhoaHoc")
      .then((res) => setMucKhoaHoc(res.data))
      .catch((erorr) => {
        console.log("erorr", erorr);
      });
  }, []);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  const uploadProps = {
    multiple: false,
    action: "https://localhost:3000/",
    accept: ".png,.jpg,.doc",
    listType: "picture",
    beforeUpload: (file) => {
      return false;
    },
  };

  return (
    <div className="flex w-full items-center mt-6">
      <div className="ml-12">
        <Button
          size="large"
          className="bg-green-500"
          type="primary"
          onClick={showModal}
        >
          Thêm khóa học
        </Button>
      </div>

      <div className="flex items-center w-full gap-24">
        <Input
          onChange={onSearchChange}
          value={searchValue}
          className="w-1/2 ml-auto rounded"
          size="large"
          placeholder="Nhập vào khóa học cần tìm"
        />
        <div className="flex items-center gap-5 loginInfo mr-14">
          <span className="text-lg font-bold">Xuân Đông</span>

          <Dropdown menu={{ items }}>
            <a href="/" onClick={(e) => e.preventDefault()}>
              <Space>
                <img
                  className="rounded-full "
                  src="https://cdn.dribbble.com/users/2364329/screenshots/6676961/02.jpg?compress=1&resize=800x600"
                  width={70}
                  height={90}
                  alt=""
                />
                <DownOutlined />
              </Space>
            </a>
          </Dropdown>
        </div>
      </div>

      <Modal
        width={1000}
        title="Thêm khóa học"
        open={isModalOpen}
        // onOk={handleOk}
        onCancel={handleCancel}
        footer={
          <Space>
            <Button
              id="btn_close"
              className="bg-red-600 text-white hover:text-white"
              onClick={handleCancel}
            >
              Đóng
            </Button>
            <Button
              form="form_add_cource"
              type="primary"
              htmlType="submit"
              id="btn_add"
              className="bg-green-600 text-white hover:text-white"
              onClick={onFinish}
            >
              Thêm khóa học
            </Button>
          </Space>
        }
      >
        <div className=" ">
          <Form
            onValuesChange={handleChange}
            className=" grid grid-cols-2 gap-7"
            form={formAdd}
            name="form_add_cource"
            wrapperCol={{
              span: 24,
            }}
            style={{
              maxWidth: 955,
            }}
            autoComplete="off"
          >
            <div>
              <Form.Item
                name="maKhoaHoc"
                value={course.maKhoaHoc}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập mã khóa học!",
                  },
                ]}
              >
                <Input
                  name="maKhoaHoc"
                  prefix={<UserOutlined className="site-form-item-icon" />}
                  placeholder="Mã khóa học"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="maDanhMucKhoaHoc"
                value={course.maDanhMucKhoaHoc}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn danh mục khóa học!",
                  },
                ]}
              >
                <Select
                  name="maDanhMucKhoaHoc"
                  size="large"
                  prefix={<TeamOutlined className="site-form-item-icon" />}
                  placeholder="Danh mục khóa học"
                >
                  {muckhoahoc.map((item) => (
                    <React.Fragment key={item.id}>
                      <Option value={item.maDanhMuc}>
                        {item.tenDanhMuc} - {item.maDanhMuc}
                      </Option>
                    </React.Fragment>
                  ))}
                </Select>
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="danhGia"
                value={course.danhGia}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập đánh giá!",
                  },
                ]}
              >
                <Input
                  name="danhGia"
                  placeholder="Đánh giá"
                  prefix={<StarOutlined className="site-form-item-icon" />}
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="taiKhoanNguoiTao"
                value={course.taiKhoanNguoiTao}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập người tạo!",
                  },
                ]}
              >
                <Input
                  name="taiKhoanNguoiTao"
                  prefix={<SmileOutlined className="site-form-item-icon" />}
                  placeholder="Người tạo"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="tenKhoaHoc"
                value={course.tenKhoaHoc}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập tên khóa học!",
                  },
                ]}
              >
                <Input
                  name="tenKhoaHoc"
                  prefix={<AntDesignOutlined className="site-form-item-icon" />}
                  placeholder="Tên khóa học"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="ngayTao"
                value={course.ngayTao}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập ngày tạo!",
                  },
                ]}
              >
                <Input
                  name="ngayTao"
                  prefix={<CalendarOutlined className="site-form-item-icon" />}
                  placeholder="Ngay tao"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item
                name="luotXem"
                value={course.luotXem}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập lượt xem!",
                  },
                ]}
              >
                <Input
                  name="luotXem"
                  prefix={<EyeOutlined className="site-form-item-icon" />}
                  placeholder="Lượt xem"
                />
              </Form.Item>
            </div>

            <div>
              <Form.Item name="hinhAnh" label="Hình Ảnh">
                <Upload {...uploadProps}>
                  <Button icon={<UploadOutlined />}>Click to upload</Button>
                </Upload>
              </Form.Item>
            </div>
            <div>
              <Form.Item
                name="maNhom"
                value={course.maNhom}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng chọn mã nhóm!",
                  },
                ]}
              >
                <Select
                  name="maNhom"
                  size="large"
                  prefix={<TeamOutlined className="site-form-item-icon" />}
                  placeholder="Mã nhóm "
                >
                  <Option value="GP01">GP01</Option>
                  <Option value="GP02">GP02</Option>
                  <Option value="GP03">GP03</Option>
                  <Option value="GP04">GP04</Option>
                  <Option value="GP05">GP05</Option>
                  <Option value="GP06">GP06</Option>
                  <Option value="GP07">GP07</Option>
                  <Option value="GP08">GP08</Option>
                  <Option value="GP09">GP09</Option>
                  <Option value="GP10">GP10</Option>
                  <Option value="GP11">GP11</Option>
                  <Option value="GP12">GP12</Option>
                  <Option value="GP13">GP13</Option>
                  <Option value="GP14">GP14</Option>
                  <Option value="GP15">GP15</Option>
                </Select>
              </Form.Item>
            </div>
            <div>
              <Form.Item
                name="biDanh"
                value={course.biDanh}
                rules={[
                  {
                    required: true,
                    message: "Vui lòng nhập bí danh!",
                  },
                ]}
              >
                <Input
                  name="biDanh"
                  prefix={<SmileOutlined className="site-form-item-icon" />}
                  placeholder="Bí danh"
                />
              </Form.Item>
            </div>

            <div className="w-full block">
              <div className="bg-slate-100 h-14 flex items-center">
                <p className="ml-7 font-semibold text-xl">Mô tả khóa học</p>
              </div>
              <Divider></Divider>
              <div className="flex gap-6 items-center">
                <img
                  src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/1200px-React-icon.svg.png"
                  width={220}
                  alt="aa"
                />

                <Form.Item name="moTa" value={course.moTa} className="w-full ">
                  <TextArea name="moTa" rows={9} />
                </Form.Item>
              </div>
            </div>
          </Form>
        </div>
      </Modal>
    </div>
  );
}
