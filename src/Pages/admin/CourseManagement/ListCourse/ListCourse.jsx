import React from "react";
import HeaderCourse from "../HeaderCourse/HeaderCourse";
import TableCourse from "../TableCourse/TableCourse";

export default function ListCourse() {
  return (
    <div className="rounded-md w-full ml-2 mr-6 my-4 bg-white pt-2">
      <div>
        {/* <HeaderCourse /> */}
        <TableCourse />
      </div>
    </div>
  );
}
